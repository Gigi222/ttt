package com.example.tic_tac_toe

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private var firstplayer = true
    private var Tie = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    fun restartGame() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun init() {
        Try_again.setOnClickListener {
            restartGame()
        }

        Button00.setOnClickListener {
            switchPlayer(Button00)

        }
        Button01.setOnClickListener {
            switchPlayer(Button01)

        }
        Button02.setOnClickListener {
            switchPlayer(Button02)
        }
        Button10.setOnClickListener {
            switchPlayer(Button10)

        }
        Button11.setOnClickListener {
            switchPlayer(Button11)

        }
        Button12.setOnClickListener {
            switchPlayer(Button12)

        }
        Button20.setOnClickListener {
            switchPlayer(Button20)

        }
        Button21.setOnClickListener {
            switchPlayer(Button21)

        }
        Button22.setOnClickListener {
            switchPlayer(Button22)

        }
    }

    private fun switchPlayer(button: Button) {
        if (firstplayer) {
            button.text = "X"
        } else {
            button.text = "0"
        }
        button.isClickable = false
        firstplayer = !firstplayer
        winnerChecker()

    }

    private fun winnerChecker() {
        if (Button00.text.toString()
                .isNotEmpty() && Button00.text.toString() == Button01.text.toString() && Button01.text.toString() == Button02.text.toString()
        ) {
            showToast(Button00.text.toString())
            gameEnder()
        } else if (Button10.text.toString()
                .isNotEmpty() && Button10.text.toString() == Button11.text.toString() && Button11.text.toString() == Button12.text.toString()
        ) {
            showToast(Button10.text.toString())
            gameEnder()
        } else if (Button20.text.toString()
                .isNotEmpty() && Button20.text.toString() == Button21.text.toString() && Button21.text.toString() == Button22.text.toString()
        ) {
            showToast(Button20.text.toString())
            gameEnder()
        } else if (Button00.text.toString()
                .isNotEmpty() && Button00.text.toString() == Button10.text.toString() && Button10.text.toString() == Button20.text.toString()
        ) {
            showToast(Button00.text.toString())
            gameEnder()
        } else if (Button01.text.toString()
                .isNotEmpty() && Button01.text.toString() == Button11.text.toString() && Button11.text.toString() == Button21.text.toString()
        ) {
            showToast(Button01.text.toString())
            gameEnder()
        } else if (Button02.text.toString()
                .isNotEmpty() && Button02.text.toString() == Button12.text.toString() && Button12.text.toString() == Button22.text.toString()
        ) {
            showToast(Button02.text.toString())
            gameEnder()
        } else if (Button00.text.toString()
                .isNotEmpty() && Button00.text.toString() == Button11.text.toString() && Button11.text.toString() == Button22.text.toString()
        ) {
            showToast(Button00.text.toString())
            gameEnder()
        } else if (Button02.text.toString()
                .isNotEmpty() && Button02.text.toString() == Button11.text.toString() && Button11.text.toString() == Button20.text.toString()
        ) {
            showToast(Button02.text.toString())
            gameEnder()

        } else {
            tiechecker()
        }
    }

    fun tiechecker() {
        if (
            (Button00.isClickable == Button01.isClickable)
            && (Button00.isClickable == Button02.isClickable)
            && (Button01.isClickable == Button02.isClickable)
            && (Button10.isClickable == Button11.isClickable)
            && (Button11.isClickable == Button12.isClickable)
            && (Button10.isClickable == Button12.isClickable)
            && (Button20.isClickable == Button21.isClickable)
            && (Button21.isClickable == Button22.isClickable)
            && (Button20.isClickable == Button22.isClickable)
            && (Button00.isClickable == Button22.isClickable)
            && (Button10.isClickable == Button22.isClickable)
            && (Button11.isClickable == Button22.isClickable)
        ) {
            Tie = !Tie
            tie()
        }

}


private fun tie() {
    if (Tie == true) {
        Toast.makeText(this, "It's a Tie", Toast.LENGTH_SHORT).show()
    }

}

private fun showToast(message: String) {
    Toast.makeText(this, "Winner is $message", Toast.LENGTH_SHORT).show()
}

private fun gameEnder() {
    Button00.isClickable = false
    Button01.isClickable = false
    Button02.isClickable = false
    Button10.isClickable = false
    Button20.isClickable = false
    Button11.isClickable = false
    Button12.isClickable = false
    Button22.isClickable = false
    Button21.isClickable = false

}


}
